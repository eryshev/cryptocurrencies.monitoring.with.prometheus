import mill._, scalalib._

object scraper extends ScalaModule {
  def scalaVersion = "2.12.6"
  def http4sVersion = "0.18.5"
  def logbackVersion = "1.2.3"
  def circeVersion = "0.9.3"

  def ivyDeps = Agg(
    ivy"org.http4s::http4s-dsl:$http4sVersion",
    ivy"org.http4s::http4s-blaze-server:$http4sVersion",
    ivy"org.http4s::http4s-blaze-client:$http4sVersion",
    ivy"org.http4s::http4s-circe:$http4sVersion",
    ivy"org.http4s::http4s-scala-xml:$http4sVersion",
    ivy"io.circe::circe-generic:$circeVersion",
    ivy"ch.qos.logback:logback-classic:$logbackVersion"
  )

  // needed for http4s
  def scalacOptions = Seq("-Ypartial-unification")
}