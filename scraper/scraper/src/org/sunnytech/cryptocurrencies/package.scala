package org.sunnytech

import cats.effect._

import io.circe.{Decoder, HCursor}

import org.http4s.{EntityEncoder, EntityDecoder}
import org.http4s.circe._

import io.circe.generic.semiauto._

import org.slf4j.{Logger, LoggerFactory}


package object cryptocurrencies {
  val logger = LoggerFactory.getLogger(this.getClass)

  sealed trait Currency { val code: String }
  case object Bitcoin extends Currency { val code = "BTC" }
  case object Litecoin extends Currency { val code = "LTC" }
  case object Ethereum extends Currency { val code = "ETH" }

  case class Ticker(timestamp: Long, price: Double) {
    def asString(): String = s"$timestamp,$price"

    def asBytes(): Array[Byte] = ???
  }

  object Ticker {
    private implicit val circeDecoder = new Decoder[Ticker] {
      final def apply(c: HCursor): Decoder.Result[Ticker] =
        for {
          timestamp <- c.downField("metadata").downField("timestamp").as[Long]
          price <- c.downField("data").downField("quotes").downField("EUR").downField("price").as[Double]
        } yield Ticker(timestamp, price)
    }
    implicit def http4sDecoder: EntityDecoder[IO, Ticker] = jsonOf[IO, Ticker]
  }

  case class AdminCurrency(price: Double)

  object AdminCurrency {
    private implicit val circeDecoder = deriveDecoder[AdminCurrency]
    implicit def http4sDecoder: EntityDecoder[IO, AdminCurrency] = jsonOf[IO, AdminCurrency]
  }
}
