package org.sunnytech.cryptocurrencies

import cats.effect._

import fs2.{Stream => Stream}
import fs2.async.immutable.Signal

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.FiniteDuration

trait Puller {
  protected val store: IStore
  protected val frequency: FiniteDuration
  protected val client: Client

  private val requestInterrupt =
    fs2.async.signalOf[IO, Boolean](false).unsafeRunSync

  private lazy val pricesByCurrency = for {
    bitcoin <- client.get(Bitcoin)
    litecoin <- client.get(Litecoin)
    ethereum <- client.get(Ethereum)
  } yield
    Map(
      Bitcoin -> bitcoin,
      Litecoin -> litecoin,
      Ethereum -> ethereum
    )

  private lazy val pullAndStore = Stream
    .eval_(
      pricesByCurrency
        .map(_.map {
          case (currency, ticker: Ticker) =>
            store.set(currency, ticker.price)
            logger.info("prices have been updated")
        }))
    .handleErrorWith(e =>
      Stream.eval_(IO(logger.warn(
        s"prices haven't been updated because '${e.getMessage()}'"))))

  private lazy val schedulerStream = fs2.Scheduler[IO](1)
  private lazy val pullerStream = pullAndStore ++
    (for {
      scheduler <- schedulerStream
      _ <- scheduler.awakeEvery[IO](frequency)
      _ <- {
        logger.info("stream's awaken")
        pullAndStore
      }
    } yield ())

  private lazy val interruptablePuller = pullerStream.interruptWhen(requestInterrupt).compile.last

  def start() = {
    requestInterrupt.set(false).flatMap(_ => interruptablePuller).runAsync {
      case Left(e) =>
        IO {
          logger.error("critical error during pulling, puller's going to stop",
                       e)
        }
      case Right(_) =>
        IO {
          logger.info(
            "puller's stream has been interrupted, puller's going to stop")
        }
    }.unsafeRunAsync(_ => Unit)
  }

  def stop() = {
    requestInterrupt.set(true).unsafeRunAsync(_ => Unit)
  }
}

class CoinMarketCapPuller(protected val store: IStore,
                          protected val frequency: FiniteDuration)
    extends Puller {
  protected val client = CoinMarketCapClient
}

class FakePuller(protected val store: IStore,
                 protected val frequency: FiniteDuration)
    extends Puller {
  protected val client = FakeClient()
}
