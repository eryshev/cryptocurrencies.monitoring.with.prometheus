package org.sunnytech.cryptocurrencies

import cats.effect.IO

import org.http4s.Http4s._
import org.http4s.client.middleware.FollowRedirect
import org.http4s.client.blaze.Http1Client

import org.sunnytech.cryptocurrencies

trait Client {
  def get(currency: Currency): IO[Ticker]
}

object CoinMarketCapClient extends Client {
  private val client = FollowRedirect(1)(Http1Client[IO]().unsafeRunSync())

  private val currency2Id = Map[Currency, Int](
    Bitcoin -> 1,
    Litecoin -> 2,
    Ethereum -> 1027
  )

  private def currency2Url(currency: Currency) =
    s"https://api.coinmarketcap.com/v2/ticker/${currency2Id(currency)}?convert=EUR"

  def get(currency: Currency): IO[Ticker] =
    IO(currency2Url(currency))  
      .flatMap { (url: String) =>
        logger.info(s"get $url")
        client.expect[Ticker](url)
      }

}

class FakeClient(seed: Map[Currency, Ticker]) extends Client {
  def get(currency: Currency): IO[Ticker] =
    IO {
      seed(currency)
    }
}

object FakeClient {
  def apply() = new FakeClient(Map(
    Bitcoin -> Ticker(-1, 5400),
    Litecoin -> Ticker(-1, 90),
    Ethereum -> Ticker(-1, 450)
  ))
}
