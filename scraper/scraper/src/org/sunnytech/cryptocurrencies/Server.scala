package org.sunnytech.cryptocurrencies

import cats.effect._

import fs2.{Stream, StreamApp, Scheduler}
import fs2.StreamApp.ExitCode

import org.http4s._
import org.http4s.dsl.io._
import org.http4s.server.blaze._
import org.http4s.scalaxml._

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.xml.XML
import scala.xml.dtd.{DocType, NoExternalID, NotationDecl}
import scala.util.Try

object Server extends StreamApp[IO] {
  private val store: IStore = new Store()

  private val fakePuller = new FakePuller(store, 2.minutes)
  private val coinMarketCapPuller = new CoinMarketCapPuller(store, 2.minutes)

  private object CurrencyVar {
    def unapply(str: String): Option[Currency] = Try(str match {
      case "BTC" => Bitcoin
      case "LTC" => Litecoin
      case "ETH" => Ethereum
      case _ => ???
    }).toOption
  }
  
  private val docType = DocType("Thank_you_for_coming", NoExternalID, Seq.empty)
  private def static(file: String, request: Request[IO]) =
    StaticFile.fromResource(s"/$file", Some(request)).getOrElseF(NotFound())

  private val v1Service = HttpService[IO] {
    case GET -> Root / "ticker" / CurrencyVar(currency) =>
      logger.debug(s"currency is $currency")
      store.get(currency).map { price =>
          <currency>
            <name>{currency.toString}</name>
            <code>{currency.code}</code>
            <price>{price}</price>
          </currency>
      }.map(xml => Ok(xml)).getOrElse(InternalServerError("There isn't any price in the store, maybe did you start any puller?"))
    case GET -> Root / "ticker" / currency =>
      BadRequest(s"We don't provide a ticker for currency '$currency'")
  }

  private val adminService = HttpService[IO] {
    case body @ PUT -> Root / CurrencyVar(currency) =>
      for {
        cur <- body.as[AdminCurrency]
        _ <- IO {
          logger.warn(s"set $currency price at ${cur.price}")
          store.set(currency, cur.price)
        }
        resp <- Ok("Ok")
      } yield resp

    case GET -> Root / "coinmarketcap" / "start" =>
      coinMarketCapPuller.start()
      Ok("Ok")
    case GET -> Root / "fake" / "start" =>
      fakePuller.start()
      Ok("Ok")
    case GET -> Root / "coinmarketcap" / "stop" =>
      coinMarketCapPuller.stop()
      Ok("Ok")
    case GET -> Root / "fake" / "stop" =>
      fakePuller.stop()
      Ok("Ok")
  }

  private val funService = HttpService[IO] {
    case request @ GET -> Root / path if path.endsWith(".svg") => static(path, request)
    case req @ _ -> _ => static("index.html", req).map(res =>
      res.copy(status = Status.NotFound)
    )
  }

  private val blaze = BlazeBuilder[IO]
    .bindHttp(8080, "0.0.0.0")
    .mountService(v1Service, "/api/v1")
    .mountService(adminService, "/admin")
    .mountService(funService, "/")


  override def stream(args: List[String], requestShutdown: IO[Unit]): Stream[IO, ExitCode] = blaze.serve
}