package org.sunnytech.cryptocurrencies

trait IStore {
  def get(currency: Currency): Option[Double]

  def set(currency: Currency, price: Double): Unit
}

class Store(init: Map[Currency, Double] = Map.empty) extends IStore {
  private val store = collection.mutable.Map[Currency, Double](init.toSeq:_*)

  def get(currency: Currency): Option[Double] = {
    store.get(currency)
  }

  def set(currency: Currency, price: Double) = {
    store(currency) = price
  }
}