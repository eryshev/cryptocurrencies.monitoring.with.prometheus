__Launch the Docker container__:
```
docker-compose run --service-ports --rm scraper
```
Inside of the container type:
```
mill scraper.run
```
It should download some dependencies and launch an HTTP server on port 8080.
```
mill scraper.run
...
INFO  o.h.s.b.BlazeBuilder - http4s v0.18.5 on blaze v0.12.12started at http://0.0.0.0:8080/
```
Verify the port above and change it if you want by modifying `docker-compose.yml`