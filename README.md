# Monitoring your cryptocurrencies with Prometheus workshop

Welcome! 

[Prometheus](https://prometheus.io) is an easy scalable solution to monitor your infrastructure and applications.

Easy to install it can quickly start monitor your application and you don't even need to change its legacy code!

It is also a time-series database, a query language and an alert generator.
We will have a look at all these topics during the workshop.

And a hype part of the session is that we're going to apply our freshly gained knowledge to an amusing application - cryptocurrency price tracking.

I hope everyone will learn something new today and will have fun.

# Usage
## The workshop is available on GitHub pages
https://eryshev.gitlab.io/cryptocurrencies.monitoring.with.prometheus/

## Presentation on local
- open `prez/dist/presentation.html` in browser of your choice to see a presentation about this workshop
## Workshop on local
- open `workshop/output/index.html` and follow instructions to learn!

# Project anatomy
- `dockers` - a `Dockerfile` for an application which we're going to monitor, however it's completely _optional_ to use Docker
- `prez` - a workshop presentation source code
- `scraper` - a source code of an application that monitors cryptocurrency prices
- `tools` - archives of different tools for this workshop for the most popular operating systems. So you just have to `untar` them into the right directory.
- `work` - _the main working directory_ that contains a builded JAR of scraper application and empty folders for each tool which we are going to install during this workshop
- `workshop` - workshop's microsite source code

# Dev
## Commands

All commands must be run from root directory of this repository

- generate the presentation with [backslide](https://github.com/sinedied/backslide)

```
cd prez
bs serve
```

- generate the workshop with [markdown-styles](https://github.com/mixu/markdown-styles)

```
cd workshop
generate-md --layout ./layout \
    --highlight-spoiler ./layout/highlighters/spoiler.js
```

- build scraper with [mill](https://github.com/lihaoyi/mill)

```
cd scraper
mill scraper.assembly
cp out/scraper/assembly/dest/out.jar ../work/scraper/scraper.uber.jar
```

- run builded app

```
cd work/scraper
java -cp scraper.uber.jar org.sunnytech.cryptocurrencies.Server
```


