#!/usr/bin/env python3

# This exporter recurrently asks a scraper application about cryptocurrencies prices
# and exposes it in Prometheus format https://prometheus.io/docs/concepts/data_model/
# So exporter consists of 2 parts:
# 1. Puller - a process that pulls the metrics
# 2. Exporter - an HTTP server that export the metrics

# You will need to use 3 libraries to perform a task:
# 1. Requests http://docs.python-requests.org/en/master/ - to perform HTTP requests :)
# 2. XML etree - https://docs.python.org/3/library/xml.etree.elementtree.html - to parse scraper's XML responses, 
# remember that our old application gives us XMLs
# 3. Prometheus client https://github.com/prometheus/client_python - to prepare our metrics for Prometheus, 
# it also provides a simple HTTP server to expose them

# You can install missing dependencies by running
# pip3 install prometheus_client requests

# You can run this application everywhere with Python3 installed
# On Linux/Max just run ./scraper_exporter.py
# Make sure to check that it works by opening http://localhost:8000

from prometheus_client import start_http_server, Gauge
from requests import get
from xml.etree import ElementTree
from time import sleep
from threading import Thread

# Ticker returned by scraper application
class Ticker():
    def __init__(self, code, name, price):
        self.code = code
        self.name = name
        self.price = float(price)

    def __str__(self):
        return """
        Ticker:
            Code: {}
            Name: {}
            Price: {}
        """.format(self.code, self.name, self.price)

# Print request's response for debugging
def print_response(response):
    print("""
        Status: {}
        Body: {}
    """.format(response.status_code, response.text))


if __name__ == '__main__':
    # You need to replace ??? in following functions by your code

    # This function must return a full HTTP URL encoded as string for a given currency code
    # You can find needed URL in Part 1 of this workshop
    def get_url(code):
        ???

    # This function must return a Ticker value for a given currency code.
    def get_ticker(code):
        # Build an URL for code
        url = ???

        # Use request to get a response from url
        response = ???
        print_response(response)

        # Create an XML tree from response's body
        tree = ???
        
        # Iterate over children of tree and create a Python dict
        tree_dict = {}
        for child in tree:
            # Put a value for each tag into tree_dict
            # tag: name, code, price
            

        # **dict syntax allows us to use a dict as keyword parameters
        return Ticker(**tree_dict)
        

    # Function that will be launched as separate thread and recurrently pull a ticker from scraper application
    def puller(frequency):
        # We're going export a Gauge metric, a single numerical value that can arbitrarily go up and down. 
        # You can find more details at https://prometheus.io/docs/concepts/metric_types/#gauge

        # Create an instance of Gauge collector from prometheus_client 
        # name it as "scraper_crypto_price_euros" and put a description of your choice
        ???

        # yep, poor man's "recurrently" is an infinite loop with a sleep
        while True:
            print("Pull metrics")
            
            # Get the Bitcoin's ticker
            ticker = ???
            print(ticker)

            # Set/update the ticker's price of Gauge collector
            ???

            # zzzz...
            sleep(frequency)

    # Start up the puller
    kwargs = {
        "frequency": 10,
    }
    thread = Thread(target = puller, kwargs=kwargs)
    thread.start()

    # Start up the server to expose the metrics
    start_http_server(8000)

    # We run our puller as daemon process so we need this pretty loop to block the main thread :)
    while True:
        sleep(5)