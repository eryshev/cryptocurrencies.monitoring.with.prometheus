title: Monitoring your cryptocurrencies with Prometheus
class: animation-fade
layout: true

<!-- This slide will serve as the base layout for all your slides -->
<!-- .bottom-bar[
  {{title}}
] -->

---

class: impact
background-image: url(images/landing_img.jpg)

.who-box[
  .row.table.middle.left[
  .col-9.small[
  Alexey Eryshev<br>
  ]
  .col-3.right.small[
  .responsive[![criteo logo](images/criteo.svg)]
  ]
  ]
]
---
class: center, middle

.head.dark[
# .logo[![](images/prometheus_logo.png)] ⚙ Before we begin
]
.head-spacer[]

## Join Slack
## #[mccwp](https://sunnytechevent.slack.com/)

---
class: center, middle

.head.dark[
# .logo[![](images/prometheus_logo.png)] ⚙ Before we begin
]
.head-spacer[]

## Clone

https://gitlab.com/eryshev/cryptocurrencies.monitoring.with.prometheus

.center[.alt-bg[Please start cloning 🙏]]
.center[.small.thin[USB drive with workshop is provided for those who need it]]
---
class: impact
background-image: url(images/bob.jpg)
--
![here](images/currencies.png)
---
class: impact, big-text

# ☝️ HDD ???
--

## Hype Driven Development

---
class: center, middle

.head.dark[
# .logo[![](images/prometheus_logo.png)] Intro
]
.head-spacer[]

## An open-source service system monitoring and time series database - https://prometheus.io/

???
https://confluence.criteois.com/display/OBS/Prometheus+FAQ
- collects metrics from configured targets at given intervals
- evaluates rule expressions
- displays the results
- triggers alerts if some condition is observed to be true
---
class: center, middle

.head.dark[
# .logo[![](images/stack.png)] Story
]
.head-spacer[]

![here](images/workshop_schema_before.png)
---
class: center, middle

.head.dark[
# .logo[![](images/stack.png)] Story
]
.head-spacer[]

![here](images/workshop_schema_after.png)
---
class: impact

background-image: url(images/letsdoit.jpg)
---
class: center, middle

.head.dark[
# .logo[![](images/stack.png)] Story
]
.head-spacer[]

## go Go GO

https://eryshev.gitlab.io/cryptocurrencies.monitoring.with.prometheus/
---
class: center, middle

.head.dark[
# ⌛ Timing
]

Chapter | Timing
--- | ---
Part 1 | 10 min
Part 2 | 10 min
Part 3 | 10 min
Part 4 | 20 min
Part 5 | 20 min
Part 6 | 20 min
---
class: impact, big-text

# Questions? ☝️
---
.head[
# 🔗 Some links
]
.head-spacer[]

- https://56k.cloud/blog/how-to-monitor-cryptocurrencies-with-docker--prometheus/
- https://github.com/bonovoxly/coinmarketcap-exporter
- https://docs.google.com/document/d/199PqyG3UsyXlwieHaqbGiWVa8eMWi8zzAn0YfcApr8Q
- https://prometheus.io/docs/introduction/comparison/
- https://github.com/stefanprodan/dockprom
---
class: impact, big-text

# Thank you 🙇

