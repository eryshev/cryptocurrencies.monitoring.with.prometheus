title: Monitoring your cryptocurrencies with Prometheus
home: index.html
prev: part3.html
next: part5.html
---
### Part 4
You might have already forgotten about our small cryptocurrencies application.
In this part we are finally going to use Prometheus and Grafana, to monitor it!

As we don't really have access to the source code of this application (to instrument it) or we're lazy and don't want to code anything, we are going to use an exporter.

There are a lot of exporters [all over the Internet](https://prometheus.io/docs/instrumenting/exporters/), official and not, but unfortunately nobody has ever written an exporter for our legacy application (not exactly true as I did that). So we need to create one, let's call it "scraper_exporter".
I provide a skeleton for Python, as I think it's easy enough even for people who don't use it everyday 🙊

#### _😅 4.1. Create an exporter for Bitcoin price_

- Open `work/exporter/scraper_exporter.py` and follow the instructions in the file.
- After finishing it you should see something like that at `http://localhost:8000/`

```
# HELP scraper_crypto_price_euros Price of cryptocurrency
# TYPE scraper_crypto_price_euros gauge
scraper_crypto_price_euros 5282.5183216903
```

Note that Prometheus has several rules/conventions regarding metric names so you need to pay attention when you name your metrics. More details are available on https://prometheus.io/docs/practices/naming/

```spoiler
You may find a full source code for this part [here](assets/custom/part4.1_scraper_exporter.py)
```

#### _😅4.2. Add Ethereum and Litecoin prices to exported metrics_

So far we only export one metric. In this part we will add a Prometheus label `code` that will add one additional dimension to our metric.

- Open created on the previous step `work/exporter/scraper_exporter.py`
- Add a label `code` to existing Gauge collector
- Modify the `puller` code, so that it exposes Ethereum and Litecoin prices as the same metric

🙋 Hint
```spoiler
- create a list of codes
- iterate over this list and for each code get a ticker and set a metric
- set a metric via `labels` method of Gauge collector
- use a currency code as a value of metric's label
```

🙋 You should see:
```spoiler
# HELP scraper_crypto_price_euros Price of cryptocurrency
# TYPE scraper_crypto_price_euros gauge
scraper_crypto_price_euros{code="BTC"} 5275.6610808726
scraper_crypto_price_euros{code="ETH"} 406.180088287
scraper_crypto_price_euros{code="LTC"} 71.6705095787
---
text
```

Answer
```spoiler
You may find a full source code for this part [here](assets/custom/part4.2_scraper_exporter_with_labels.py)
```

#### _😅 4.3. Add a new target to Prometheus_

We need to add this brand new exporter to Prometheus. Come on, you know how to do it! And if you don't, then take a look at the Part 2 of this workshop.

```spoiler
# scraper_exporter monitoring
- job_name: scraper_exporter
static_configs:
    - targets: ['localhost:8000']
---
yml
```

#### _😅 4.4. Create a graph panel that shows us the instant price of a currency with its history_

- create a _Graph_ panel for Bitcoin price
- see http://docs.grafana.org/reference/templating/ about Grafana variables
- specify your dashboard with the currency code variable in order to navigate easily between metrics by currency
![here](assets/custom/part4.3_btc_price.png)


🙋 Hint
```spoiler
Template query is `label_values(scraper_crypto_price_euros, code)`
```

Answer
```spoiler
You can find a valid dashboard in JSON format [here](assets/custom/part4.4_dashboard.json)
```