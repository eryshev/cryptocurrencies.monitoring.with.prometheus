title: Monitoring your cryptocurrencies with Prometheus
home: index.html
prev: part6.html
next: finish.html
---

🎉🎉🎉 Good job, you finished this workshop! 🎉🎉🎉

We've touched significant part of Prometheus eco-system, assembled it together and been able to send a notification in Slack!

And all this in less than 100 minutes!

![cat](assets/custom/prometheus_cat.jpeg)
