title: Monitoring your cryptocurrencies with Prometheus
home: index.html
prev: part5.html
next: finish.html
---
### Part 6
Ok, now we have these beautiful graphs and we can monitor the prices all day long. But what if we need to drink some coffee or catch some sleep? What if the price suddenly goes down and we need to sell some of our currencies in a rush? The answer to all these questions is alerting!

Alerting with Prometheus is separated into two parts. Alerting rules in Prometheus servers send alerts to Alertmanager. [Alertmanager](https://prometheus.io/docs/alerting/alertmanager/) then manages those alerts, including silencing, inhibition, aggregation and sending out notifications via methods such as email or Slack.
<img src="assets/custom/promethus_alerting_schema.png" width="576" height="400">
Let's start by defining some alert rules in Prometheus!

#### _😅 6.1. Create alert rules_

Documentation: 
- https://prometheus.io/docs/prometheus/latest/configuration/recording_rules/
- https://prometheus.io/docs/prometheus/latest/configuration/alerting_rules/

What you will need to do:
1. Create a file `price.rules.yml' in your Prometheus home directory.
Check this file with dedicated tool promtool which comes with your Prometheus installation.
```
./promtool check rules price.rules.yml
```
2. Add the following snippet to `price.rules.yml'.
```yml
groups:
  - name: price
    rules:
      # The name of the time-series to output to. Must be a valid metric name.
      - alert: <TODO>

        # The PromQL expression to evaluate. Every evaluation cycle this is
        # evaluated at the current moment, and the result is recorded as a new set of
        # time-series with the metric name as given by 'record'.
        expr: <TODO>

        # Alerts are considered firing once they have been returned for this long.
        # Alerts which have not yet fired for long enough are considered pending.
        for: <TODO>
```
3. Create an alert name, make sure it respects https://prometheus.io/docs/practices/rules/
```spoiler
Something as `code:scraper_crypto_price_euros:BTC:min5m` is possible
```

4. Create an alert expression that fires an alert when minimum of price for Bitcoin is greater than 5000 during the last 5 minutes.
```spoiler
min_over_time(scraper_crypto_price_euros{code='BTC'}[5m]) < 5000
```

5. Let's fire this alert if alert rule has been returning the true value for at least 10 seconds.
```spoiler
for: 10s
```

6. Make sure that your configuration is valid by using `promtool`.
```
Checking price.rules.yml
  SUCCESS: 1 rules found
```

7. Add `rule_files` to Prometheus config and restart your instance.
Now you should be able to see a created alert in "Alerts" tab of your Prometheus interface (of course if Bitcoin costs > 5000 euros :))
<div class="spoiler-container"><input type="checkbox" class="spoiler" id="spoiler-22"><label for="spoiler-22">Show answer</label><div class="spoiler-content"><div class="yml"><pre class="hljs"><code><span class="hljs-preprocessor"># rule files globs</span>
<span class="hljs-label">rule_files:</span>
  - <span class="hljs-string">"price.rules.yml"</span>
</code></pre></div></div></div>

8. Add similar rules for Ethereum and Litecoin.
<div class="spoiler-container"><input type="checkbox" class="spoiler" id="spoiler-22"><label for="spoiler-22">Show answer</label><div class="spoiler-content"><div class="yml"><pre class="hljs"><code>groups:
  - name: price
    rules:
      # The name of the time-series to output to. Must be a valid metric name.
      - alert: code:scraper_crypto_price_euros:BTC:min5m

        # The PromQL expression to evaluate. Every evaluation cycle this is
        # evaluated at the current moment, and the result is recorded as a new <span class="hljs-operator"><span class="hljs-keyword">set</span> <span class="hljs-keyword">of</span>
        # <span class="hljs-keyword">time</span>-series <span class="hljs-keyword">with</span> the metric <span class="hljs-keyword">name</span> <span class="hljs-keyword">as</span> given <span class="hljs-keyword">by</span> <span class="hljs-string">'record'</span>.
        expr: min_over_time(scraper_crypto_price_euros{code=<span class="hljs-string">'BTC'</span>}[<span class="hljs-number">5</span><span class="hljs-keyword">m</span>]) &lt; <span class="hljs-number">5000</span>

        # Alerts <span class="hljs-keyword">are</span> considered firing once they have been returned <span class="hljs-keyword">for</span> this <span class="hljs-keyword">long</span>.
        # Alerts which have <span class="hljs-keyword">not</span> yet fired <span class="hljs-keyword">for</span> <span class="hljs-keyword">long</span> enough <span class="hljs-keyword">are</span> considered pending.
        <span class="hljs-keyword">for</span>: <span class="hljs-number">10</span>s

      - alert: code:scraper_crypto_price_euros:ETH:min5m
        expr: min_over_time(scraper_crypto_price_euros{code=<span class="hljs-string">'ETH'</span>}[<span class="hljs-number">5</span><span class="hljs-keyword">m</span>]) &lt; <span class="hljs-number">400</span>
        <span class="hljs-keyword">for</span>: <span class="hljs-number">10</span>s

      - alert: code:scraper_crypto_price_euros:LTC:min5m
        expr: min_over_time(scraper_crypto_price_euros{code=<span class="hljs-string">'LTC'</span>}[<span class="hljs-number">5</span><span class="hljs-keyword">m</span>]) &lt; <span class="hljs-number">80</span>
        <span class="hljs-keyword">for</span>: <span class="hljs-number">10</span>s
</span></code></pre></div></div></div>

#### _😅 6.2. Launch AlertManager_
1. Launch it
```
cd work/dalertmanager/alertmanager-0.15.0.darwin-amd64/
./alertmanager
```

2. Configure Prometheus to send metrics to this AlertManager and restart it
```yml
alerting:
    alertmanagers:
    - scheme: http
      static_configs:
      - targets: ["localhost:9093"]
```
3. Now you might see any triggered alert in AlertManager at http://localhost:9093 

#### _😅 6.3. Configure AlertManager to send Slack notifications_ 

1. Replace `work/dalertmanager/alertmanager-0.15.0.darwin-amd64/alertmanager.yml` by following snippet
<pre class="hljs"><code><span class="hljs-attr">global:</span>
<span class="hljs-attr">  slack_api_url:</span> &lt;slack-webhook-url<span class="hljs-string">&gt;
</span><span class="hljs-attr">route:</span>
<span class="hljs-attr">  receiver:</span> <span class="hljs-string">'slack-notifications'</span>
<span class="hljs-attr">  group_by:</span> [alertname, code]
<span class="hljs-attr">receivers:</span>
<span class="hljs-attr">- name:</span> <span class="hljs-string">'slack-notifications'</span>
<span class="hljs-attr">  slack_configs:</span>
<span class="hljs-attr">  - channel:</span> <span class="hljs-string">'#mccwp'</span>
<span class="hljs-attr">    text:</span> <span class="hljs-string">'First Prometheus notification from &lt;your name&gt;'</span></code></pre>

2. Find a Slack webhook and paste it to `slack_api_url` (ask me about that) 
3. Replace `your name` by your name
4. Relaunch AlertManager
5. You can either set a threshold that triggers an alert or use the route to set a fake price
```
curl -X PUT \
  http://localhost:8080/admin/BTC \
  -H 'content-type: application/json' \
  -d '{
    "price": 1200.123
  }'
```

#### 6.4 Final Config
Final configuration for this part can be found in these files.
1. [prometheus.yml](assets/custom/part6_prometheus.yml)
2. [prometheus.price.rules.yml](assets/custom/part6_price.rules.yml)
3. [alertmanager.yml](assets/custom/part6_alertmanager.yml)