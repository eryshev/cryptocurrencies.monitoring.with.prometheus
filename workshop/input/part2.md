title: Monitoring your cryptocurrencies with Prometheus
home: index.html
prev: part1.html
next: part3.html
---
### Part 2
So far our Prometheus only monitors itself, not so useful, right?

Let's configure it to monitor the resources of a host machine.

For this task we're going to use an official exporter [node_exporter](https://github.com/prometheus/node_exporter) on OS X and Linux or
[wmi_exporter](https://github.com/martinlindhe/wmi_exporter) on Windows.

>An exporter is a binary that exposes Prometheus metrics commonly by converting them from non-Prometheus format into a format that Prometheus supports.

#### _😅 2.1. Launch the exporter_
```
cd work/node_exporter/node_exporter-0.16.0.linux-amd64
./node_exporter
```

On Windows
```
cd work/wmi_exporter
./wmi_e\xporter.e\xe --collectors.enabled "os"
```

If you're running Docker, attach yourself to a container and run a command inside
```spoiler
docker exec -ti $(docker ps -l -q) /bin/bash
/root/node_exporter/node_exporter-0.16.0.linux-amd64/node_exporter
```

It will launch an http server that exposes a bunch of metrics at the port 9100. Let's look at these metrics :

http://localhost:9100/metrics

You should see something like this:
```
# HELP node_cpu_seconds_total Seconds the cpus spent in each mode.
# TYPE node_cpu_seconds_total counter
node_cpu_seconds_total{cpu="0",mode="idle"} 140489.63
node_cpu_seconds_total{cpu="0",mode="iowait"} 12.25
node_cpu_seconds_total{cpu="0",mode="irq"} 0
node_cpu_seconds_total{cpu="0",mode="nice"} 0
node_cpu_seconds_total{cpu="0",mode="softirq"} 72.68
node_cpu_seconds_total{cpu="0",mode="steal"} 0
node_cpu_seconds_total{cpu="0",mode="system"} 2293.61
node_cpu_seconds_total{cpu="0",mode="user"} 1601.03
node_cpu_seconds_total{cpu="1",mode="idle"} 144342.17
node_cpu_seconds_total{cpu="1",mode="iowait"} 13.22
node_cpu_seconds_total{cpu="1",mode="irq"} 0
node_cpu_seconds_total{cpu="1",mode="nice"} 0
node_cpu_seconds_total{cpu="1",mode="softirq"} 9.11
node_cpu_seconds_total{cpu="1",mode="steal"} 0
node_cpu_seconds_total{cpu="1",mode="system"} 2185.7
node_cpu_seconds_total{cpu="1",mode="user"} 1610.11
```

Prometheus fundamentally stores all data as time-series: streams of timestamped values belonging to the same metric and the same set of labelled dimensions. Besides stored time-series, Prometheus may generate temporary derived time-series as the result of queries.

For a given metric name and a set of labels, time-series are frequently identified using this notation:
```
<metric name>{<label name>=<label value>, ...}
```

More details are available in the [data model documentation](https://prometheus.io/docs/concepts/data_model/).

_😅 Configure Prometheus to monitor the node with 5 seconds interval_

```spoiler
  # you should add this config to prometheus.yml
  # node monitoring
  - job_name: 'node'
    static_configs:
    # linux, os x
    - targets: ['localhost:9100']
    # windows
    - targets: ['localhost:9182']
    scrape_interval:     5s
---
yml
```

_😅 Create two graphs for total and free memory reported by node exporter_

Note that on Windows you can access only the total memory.

```spoiler
On Linux you're looking for:
- node_memory_MemTotal_bytes
- node_memory_MemFree_bytes

On OS X:
- node_memory_bytes_total
- node_memory_free_bytes_total

On Windows:
- wmi_cs_physical_memory_bytes
```