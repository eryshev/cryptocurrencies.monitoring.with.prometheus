title: Monitoring your cryptocurrencies with Prometheus
home: index.html
prev: index.html
next: part2.html
---
### Part 1
#### _😅 1.1. Prepare the application_

Let's start.

I assume there that you've setup tools mentioned [here](index.html#_%F0%9F%98%85-setup-tools_)

Launch scraper on JVM:
```
cd work/scraper
java -cp scraper.uber.jar org.sunnytech.cryptocurrencies.Server
```

You should see the following in your terminal
```
...
INFO  o.h.s.b.BlazeBuilder - http4s v0.18.5 on blaze v0.12.12started at http://0.0.0.0:8080/
```

[OPTIONAL] You can also run via Docker
```spoiler
docker-compose run --service-ports --rm scraper

Inside of the container type `mill scraper.run`

It should download some dependencies and launch an HTTP server on port 8080.
```

So now when our application is running, let's try it out by opening 

http://localhost:8080/

You should see a beautiful Sunny Tech logo and 404 error. This is an error page. 😲 We will see some valid routes below.

The application consist of a web server and a process that queries [CoinMarketCap](https://coinmarketcap.com) with a 2 minutes frequency by default.
Yes, we don't do a high frequency trading here, so it's more than enough for our task (anyway [CoinMarketCap](https://coinmarketcap.com) refreshes its data once in a 5 minutes).

It gets an object called a ticker that represents a current state of cryptocurrency and exposes its price in XML format.

You can take a look at the [documentation page](https://coinmarketcap.com/api/). 

Our app supports only 3 currencies at the moment:
- Bitcoin(API code is BTC)
- Litecoin(API code is LTC)
- Ethereum(API code is ETH)

The pull process isn't launched by default. To see how to launch it look to the next section.

#### 1.2. Scraper API

_😅 Start Coinmarketcap puller_ 

http://localhost:8080/admin/coinmarketcap/start

If everything is OK, you get a "200 OK" response and now you can see the prices.

-----

_😅 Check Bitcoin price_

http://localhost:8080/api/v1/ticker/BTC

You should see something like
```xml
<currency>
    <name>Bitcoin</name>
    <code>BTC</code>
    <price>5840.1356435302</price>
</currency>
```

-----

You can stop the Coinmarketcap puller

http://localhost:8080/admin/coinmarketcap/stop

As usual "200 OK" means that everything is alright.

-----

_😱 In case of a bad internet connection_ you will have to stop `coinmarketcap` puller and launch a different one - `fake` puller.

http://localhost:8080/admin/fake/start

It's called like that because it exposes predefined fake prices.

-----

Also at any moment you can set a fake price by executing a PUT request as in this example

```
curl -X PUT \
  http://localhost:8080/admin/BTC \
  -H 'content-type: application/json' \
  -d '{
  "price": 1200.123
}'
```


#### _😅 1.3. First look at Prometheus_
Documentation: https://prometheus.io/docs/prometheus/2.3/

_😅 Go to Prometheus directory_
```
cd work/prometheus/prometheus-2.3.1.darwin-amd64
```

_😅 Open Prometheus config_

Open `prometheus.yml` file in your preferred text editor (here I use the code)
```
code work/prometheus/prometheus-2.3.1.darwin-amd64/prometheus.yml
```

Now we're going to configure Prometheus to monitor itself by modifying some config values.

You can ignore `alerting` and `rule_files` configs for now.
There are `global` config keys and a `scrape_configs` that we're interested in.

It should contain only one scrapping point - Prometheus itself and so there should be a `global.scrape_interval` entry set as 15s by default.

_😅 Override the scrape interval_

Set the `scrape_interval` for Prometheus point as 5 seconds

```spoiler
# prometheus.yml
global:
  scrape_interval:     15s
  evaluation_interval: 15s
scrape_configs:
  - job_name: 'prometheus'

    static_configs:
    - targets: ['localhost:9090']
    
    # 1. your overriden scrape_interval
    scrape_interval: 5s
---
yml
```

_😅 Check the state of Prometheus config_
```
./promtool check config prometheus.yml
```

It must be a SUCCESS
```
Checking prometheus.yml
  SUCCESS: 0 rule files found
```

Run this tool every time when you modify that config.

_😅 Launch Prometheus_

```
./prometheus
```
It should start a web server at

http://localhost:9090/

The UI consist of 3 views:
- Alerts 
- Graph
- Status

Take a look at them.

_😅 Confirm via UI that scrapping interval is well set at 5 seconds_
```spoiler
Look for a target config in Status panel.
```

Let's see what we can do in the Graph part.

Theres we can explore which metrics are stored in Prometheus DB.

_😅 Find a metric that shows how many heap is used by Prometheus and graph it_
```spoiler
- We need to query `go_memstats_heap_inuse_bytes` and you shoud see something like
`go_memstats_heap_inuse_bytes{instance="localhost:9090",job="prometheus"}	23977984` in Console tab. 
- To graph it, just switch to the Graph tab.
```
