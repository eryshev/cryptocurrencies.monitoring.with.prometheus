title: Monitoring your cryptocurrencies with Prometheus
home: index.html
prev: part2.html
next: part4.html
---
### Part 3
Documentation:

http://docs.grafana.org/guides/getting_started/#basic-concepts
http://docs.grafana.org/reference/export_import/

Grafana is a leading open source solution to create graphs and dashboards.

It supports Prometheus out of the box.

#### _😅 3.1. Launch Grafana_
```
cd work/grafana/grafana-5.1.4.darwin-x64
./bin/grafana-server
```

On Mac it can be installed and run via brew
```
brew update
brew install grafana
brew services start grafana
```

#### _😅 3.2. Create a Prometheus data source_
1. Click on the add data source
2. Name it as you want, "Sunny Tech Prometheus" for example
3. Select "Prometheus" as the type
4. Set the appropriate Prometheus server URL (for example, http://localhost:9090/)
5. Click "Save & Test" to save the new data source
6. Click "Back"

#### _😅 3.3. Play with Grafana_

_😅 Create a dashboard "Crypto Monitoring via Prometheus"_

_😅 Create a __SingleStat__ panel that shows if node is up or down_

- chose "Sunny Tech Prometheus" as a data source
- use `up{job='node'}` metric provided by Prometheus
- in General tab give it a proper title
- in Options tab
    - choose `Current` stat
    - tick `Background`coloring
    - put 0,1 as `Thresholds`
    - invert colors
- in Value Mappings tab map 1 to OK and 0 to KO

![here](assets/custom/node_status_grafana.png)

_😅 Create a Graph panel that shows Total and Free memory_

![here](assets/custom/node_total_free_memory.png)

🙋 Hint
```spoiler
- use metrics from [Part2](part2.html)
```

Answer
```spoiler
You can find a valid dashboard for Linux in JSON format [here](assets/custom/part3_dashboard.json)

To import it, check the [official documentation](http://docs.grafana.org/reference/export_import/).

On Windows and OS X, you will need to replace metrics for memory by corresponding ones for your platform.
```

