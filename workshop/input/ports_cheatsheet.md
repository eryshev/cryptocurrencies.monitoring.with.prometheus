All addresses by order you will need them.

Application | Address
----------- | --
Scraper | http://localhost:8080/
Prometheus | http://localhost:9090/
Node exporter | http://localhost:9100/
WMI exporter | http://localhost:9182/
Grafana | http://localhost:3000/
scraper_explorer | http://localhost:8000/
AlertManager | http://localhost:9093/
