title: Monitoring your cryptocurrencies with Prometheus
home: index.html
prev: part4.html
next: part6.html
---
### Part 5
In this part we're going to see how we can use built-in Prometheus functions in order to create new time-series that possibly give more insights about what is happening with our system.

You might find useful next section of documentation https://prometheus.io/docs/prometheus/latest/querying/basics/

We're going to produce some classic aggregate series, such as maximum and average, for the last 24 hours as well as a series of derivatives. For the last part you will need to recall from school the relative ratio formula.

#### _😅 5.1. Aggregate it, baby_
Create _SingleStat_ panels for:
- max price for last 24 hours
- average price for last 24 hours
- per-second derivative for the last 5 minutes
- relative percentage of change for last 5 minutes


![here](assets/custom/max_average_relative_change.png)

🙋 Hint
```spoiler
- look for a family of functions `*_over_time`
- look for `deriv` function
- look for `delta` and `offset` function
```

Answer
```spoiler
- max_over_time(scraper_crypto_price_euros{code='$code'}[24h])
- avg_over_time(scraper_crypto_price_euros{code='$code'}[24h])
- deriv(scraper_crypto_price_euros{code='$code'}[5m])
- (delta(scraper_crypto_price_euros{code='BTC'}[10m])/(scraper_crypto_price_euros{code='BTC'} offset 10m))*100
```

Answer
```spoiler
You can find a valid dashboard in JSON format [here](assets/custom/part5_dashboard.json)
```