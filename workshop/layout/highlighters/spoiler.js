var marked = require('marked');
var hl = require('./lib/hl.js');

var i = 0;

module.exports = function(code, lang) {
  i++;
  
  const re = /(-{3,}\n{0,1})/g
  const parts = code.split(re)
  const snippet = parts[0]
  const asLang = parts[2] || "markdown"
  const renderedCode = asLang !== "markdown"
    ? `<div class="${asLang}">${hl(snippet, asLang)}</div>`
    : marked(snippet.trim())

  return '<div class="spoiler-container"><input type="checkbox" class="spoiler" id="spoiler-' + i + '"/>' +
         '<label for="spoiler-' + i + '">Show answer</label><div class="spoiler-content">' +
         renderedCode +
         '</div></div>';
};
