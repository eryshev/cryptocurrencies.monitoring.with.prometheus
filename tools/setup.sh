#!/usr/bin/env bash
set -o xtrace

# $1 linux, windows, darwin
function setup() {
    OS=$1
    tar xvzf tools/amd64/$OS/prometheus-2.3.1.$OS-amd64.tar.gz -C work/prometheus
    tar xvzf tools/amd64/$OS/alertmanager-0.15.0.$OS-amd64.tar.gz -C work/alertmanager
    
    if [ "$OS" = "windows" ]; then
        unzip tools/amd64/$OS/wmi_exporter-amd64.zip -d work/wmi_exporter
        unzip tools/amd64/$OS/grafana-5.1.4.$OS-x64.zip -d work/grafana
    else
        tar xvzf tools/amd64/$OS/grafana-5.1.4.$OS-x64.tar.gz -C work/grafana
        tar xvzf tools/amd64/$OS/node_exporter-0.16.0.$OS-amd64.tar.gz -C work/node_exporter
    fi
}
setup $1