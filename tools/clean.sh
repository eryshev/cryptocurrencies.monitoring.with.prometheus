set -o xtrace

# $1 linux, windows, darwin
function clean() {
    OS=$1
    rm -r work/prometheus/prometheus-2.3.1.$OS-amd64
    rm -r work/alertmanager/alertmanager-0.15.0.$OS-amd64
    rm -r work/grafana/grafana-5.1.4*
    rm -r work/node_exporter/node_exporter-0.16.0.$OS-amd64
    rm -r work/wmi_exporter/wmi_exporter.exe
}
clean $1